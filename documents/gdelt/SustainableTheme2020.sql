WITH Themes AS(
  SELECT theme FROM `credit-suisse20-rem-2199.telco_persons.SustainableThemeNames` 
),
PersonsThemes AS (
SELECT
  Persons, V2Themes
FROM
  `gdelt-bq.gdeltv2.gkg_partitioned` g
JOIN
  Themes t on lower(g.V2Themes) like CONCAT('%',lower(t.theme),'%')
WHERE
  g._PARTITIONDATE > "2020-01-01" 
)    
SELECT
  REGEXP_REPLACE(person,',.*','') AS person,
  REGEXP_REPLACE(theme,',.*','') AS theme,  
  COUNT(*) AS count
FROM
  PersonsThemes p,
  UNNEST(SPLIT(Persons,';')) person,
  UNNEST(SPLIT(V2Themes,';')) theme
GROUP BY
  person, theme
ORDER BY
  count DESC