WITH
  Persons AS (
  SELECT
    CONCAT(First_Name,' ',Last_Name) AS FullName
    FROM
    `credit-suisse20-rem-2173.persons.CEOsPresAndVPs`
    )
SELECT
  g.person ,
  g.theme ,
  g.count 
FROM
  `credit-suisse20-rem-2199.telco_persons.SustainableTheme2020` g
inner join
  Persons p on lower(g.Person) = lower(p.FullName)